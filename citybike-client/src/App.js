import React, { Component } from "react";
import socketIOClient from "socket.io-client";
import { Map, TileLayer, Marker, Popup, Polygon, Circle } from "react-leaflet";
import markerIconPng from 'leaflet/dist/images/marker-icon.png'
import { Icon } from "leaflet";
import CircleBlue from './images/circleBlue.png'
import CircleRed from './images/circleRed.png'
import CircleYellow from './images/circleYellow.png'

class App extends Component {

    constructor() {
        super();

        this.state = {
            currentNetwork: 'ecobici',
            response: false,
            endpoint: "ws.citybik.es",
            lat: 38.8967584,
            lng: -77.03701629999999,
            zoom: 12,
            stations: []
        };

    }

    componentDidMount() {
        this.getDataServiceLocation(this.state.currentNetwork)

    }

    //capital-bikeshare
    getDataServiceLocation(area) {
        let url = `https://api.citybik.es/v2/networks/${area}`

        fetch(url)
            .then(response => {

                response.json().then(data => {
                    let network = data.network
                    console.log(data)
                    let stations = network.stations.map(s => {
                        let sN = { ...s }
                        sN.image = CircleBlue
                        return sN
                    })

                    this.setState({ stations: stations, lat: network.location.latitude, lng: network.location.longitude }, () => {
                        this.callSocket()
                    })
                })
            }, error => {
                console.error(error)
            })
    }

    callSocket() {
        const { endpoint } = this.state;
        const socket = socketIOClient(endpoint);

        socket.on('diff', (data) => {

            if (data.message.network === this.state.currentNetwork) {
                console.log("-------- OK OK --------")
                let stationNew = data.message.station
                let allValues = localStorage.getItem("stations") ? localStorage.getItem("stations") : []
                let stations = this.state.stations

                for (let i in stations) {
                    let st = this.state.stations[i]
                    if (st.id === stationNew.id) {
                        stationNew.image = CircleYellow
                        stations[i] = stationNew
                        this.reloadItems()
                    }
                }

                setTimeout(() => {
                    this.show()
                }, 20000)

                allValues[new Date().toLocaleTimeString()] = stations
                //localStorage.setItem("stations", JSON.stringify(allValues))

                this.setState({ stations })
            }
        })
    }

    show() {
        let data = localStorage.getItem("stations")
        console.log(JSON.parse(data))
    }

    reloadItems() {
        setTimeout(() => {
            let stations = this.state.stations.map(s => {
                s.image = CircleBlue
                return s
            })
            this.setState({ stations })
        }, 50000)
    }

    render() {
        const { stations } = this.state;
        const position = [this.state.lat, this.state.lng]
        return (

            <div className="map">
                <h1> City Bikes in Miami </h1>
                <Map center={position} zoom={this.state.zoom}>
                    <TileLayer
                        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    {
                        stations.map((s, i) => {
                            return (
                                <Marker key={i} position={[s.latitude, s.longitude]} icon={new Icon({ iconUrl: s.image, iconSize: [10, 10], iconAnchor: [12, 41] })}>
                                    <Popup>
                                        Name: {s.name} <br></br>
                                        Empty Slots: {s.empty_slots} <br></br>
                                        Free Bikes: {s.free_bikes} <br></br>
                                    </Popup>
                                </Marker>
                            )
                        })
                    }
                </Map>
            </div>
        );
    }
}
export default App;
